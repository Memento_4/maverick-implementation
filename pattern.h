#ifndef PATTERN_H
#define PATTERN_H

#include <stdbool.h>
#include "context.h"

typedef struct pattern_score{
	graph* pattern;
	float score;
}pattern_score;

void delete_last_pattern_score(list* pairs);
void add_member_sort_ps(list* pairs,pattern_score* pa);
list* pattern_generator(char* interest,graph* p,list* matches,int w, graph* g);

#endif
