#ifndef GRAPHT_H
#define GRAPHT_H
#include "node.h"
#include "edge.h"
#include "variable.h"
#include "list.h"

typedef struct graph{
	nlist* nl;
	elist* el;
	vlist* vl;
}graph;

graph* create_graph();

node* add_node_g(graph* g,char* name);

node* add_node_vg(graph* g,char* name,char* varname);

edge* add_edge_g(graph* g,char* from,char* to,char* attrname);

variable* add_variable_g(graph* g,char* name,int flag);

void delete_graph(graph* g);

void print_graph(graph* g);

graph* read_graph(char* file);

graph* copy_graph(graph* g);

int is_good_match(graph* g,graph* m);

void delete_bad_matches(list* matches,graph* g);

list* match(graph* g, graph* p);

list* find_matches(graph* g,list* matches,int num);

void delete_n_members_g(list* list,int num);

void delete_member_g(list* list,member* member);

void print_list_g(list* list);

void delete_list_g(list* list);

int find_next_qm(graph* p);

#endif
