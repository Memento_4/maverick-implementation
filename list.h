#ifndef LIST_H
#define LIST_H
#include "edge.h"
//#include "grapht.h"

typedef struct member{
	void* node;
	struct member* next;
}member;

typedef struct list{
	member* head;
	member* tail;
	int num;
}list;

list* create_list();

member* add_member(list* list,void* t);

void delete_list(list* list);

void* find_m_bynum(list* list,int num);

void print_list_edge(list* list);

void delete_member(list* list,member* mem);

void delete_member_without(list* list,member* mem);

member* find_member(list* list,void* p);

#endif
