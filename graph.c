#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "graph.h"

void readgraph(graph* g,char* file){
	FILE * fp;
	int numv,numa,nume;
	int numn;
	char name[100],attr[100],name2[100];
	fp = fopen ( file , "r" );
	fscanf(fp,"%d %d %d",&numv,&numa,&nume);
	g->v_num = numv;
	g->variables = malloc(sizeof(variable)*numv);
	g->n_num = numa;
	g->nodes = malloc(sizeof(node)*numa);
	g->e_num = nume;
	g->edges = malloc(sizeof(edge)*nume);
	int i,count=0;
	for(i=0;i<numv;i++){
		fscanf(fp,"%s %d",name,&numn);
		g->variables[i].num = numn;
		strcpy((g->variables[i]).name,name);
		g->variables[i].nodes = malloc(sizeof(node)*numn);
		int j;
		for(j=0;j<numn;j++){
			fscanf(fp,"%s",name);
			strcpy(((g->variables[i]).nodes[j]).name,name);
			strcpy(g->nodes[count].name,name);
			strcpy(g->nodes[count].varname,g->variables[i].name);
			g->nodes[count].flag =0;
			count++;
		}
	}
	for(i=0;i<nume;i++){
		fscanf(fp,"%s %s %s",name,attr,name2);
		strcpy(g->edges[i].node1.name,name);
		g->edges[i].node1.flag = 0;
		strcpy(g->edges[i].attribute,attr);
		strcpy(g->edges[i].node2.name,name2);
		g->edges[i].node2.flag = 0;
	}
	fclose(fp);

}

/*void readgraph(graph* g, char* varfile,char* graph){
	readvars(g,varfile);
}
*/

void remgraph(graph* g){
	int i;
	for(i=0;i<g->v_num;i++)
		free(g->variables[i].nodes);
	if(g->v_num > 0)
		free(g->variables);
	if(g->n_num > 0)
		free(g->nodes);
	if(g->e_num > 0);
		free(g->edges);
	free(g);

}

int find_num_nodes(graph* g,char* varname){
	int i;
	for(i=0;i<g->v_num;i++){
		if(strcmp(varname,g->variables[i].name) == 0)
			return g->variables[i].num;
	}
}

void copy_node(node* to,node* from){
	strcpy(to->name,from->name);
	strcpy(to->varname,from->varname);
	to->flag = from->flag;
}

node* create_ns_copy(node* ns,int num){
	node* cs= malloc(sizeof(node)*num);
	int i;
	for(i=0;i<num;i++){
		copy_node(&cs[i],&ns[i]);
	}
	return cs;
}

void copy_edge(edge* to,edge* from){
	copy_node(&(to->node1),&(from->node1));
	copy_node(&(to->node2),&(from->node2));
	strcpy(to->attribute,from->attribute);
}

edge* create_es_copy(edge* es,int num){
	edge* cs = malloc(sizeof(edge)*num);
	int i;
	for(i=0;i<num;i++){
		copy_edge(&cs[i],&es[i]);
	}
	return cs;
}

match create_m_copy(match m){
	match c;
	c.nodes = create_ns_copy(m.nodes,m.n_num);
	c.n_num = m.n_num;
	c.edges = create_es_copy(m.edges,m.e_num);
	c.e_num = m.e_num;
	return c;
}

variable* return_var(graph* g,char* varname){
	int i;
	for(i=0;i<g->v_num;i++){
		if(strcmp(varname,g->variables[i].name) == 0)
			return &(g->variables[i]);
	}
}

int check_if_dupl(node* ns,int num){
	int i,j;
	for(i=0;i<num;i++){
		for(j=i+1;j<num;j++){
			if (strcmp(ns[i].name,ns[j].name) == 0)
				return 1;
		}
	}
	return 0;
}

int e_cmp(edge e1,edge e2){
	if((strcmp(e1.node1.name,e2.node1.name)==0) && (strcmp(e1.node2.name,e2.node2.name)==0) && (strcmp(e1.attribute,e2.attribute) == 0))
		return 0;
	else
		return 1;
}

int check_if_suitable_match(match m,graph* g){
	int i,j;
	for(i=0;i<m.e_num;i++){
		for(j=0;j<g->e_num;j++){
			if(e_cmp(m.edges[i],g->edges[j]) == 0)
				continue;
			else
				return 1;
		}
	}
	return 0;
} 

void swap(void* m1, void* m2){
	*m1 = *m2;
}

void m_rb(matchs* ms,int n){
	int i;
	match* t = ms->matches;
	for(i=n;i<ms->num-1;i++){
		//&(t[i]) = &(t[i+1]);
		swap(&t[i],&t[i+1]);
	}
	ms->num--;
}

void rm_wr_matches(matchs* ms, graph* g){
	int i;
	for(i=0;i<ms->num;i++){
		if(check_if_dupl(ms->matches[i].nodes,ms->matches[i].n_num) == 1)
			m_rb(ms,i);
		else if(check_if_suitable_match(ms->matches[i],g) == 1){
			m_rb(ms,i);
		}
	}
}

void replacene(node* nodes,edge* edges,int n_num, int e_num){
	int i,j;
	for(i=0;i<n_num;i++){
		for(j=0;j<e_num;j++){
			if((strcmp(nodes[i].varname,edges[j].node1.varname) == 0) && (nodes[i].flag == edges[j].node1.flag)){
				strcpy(edges[j].node1.name,nodes[i].name);
			}
			if((strcmp(nodes[i].varname,edges[j].node2.varname) == 0) && (nodes[i].flag == edges[j].node2.flag)){
				strcpy(edges[j].node2.name,nodes[i].name);
			}
		}
	}
}

matchs* find_all_possible_matches(graph* g,match* matches,int num){
	int i,j,k,count=0;
	match* ms;
	int nv;
	variable* var;
	for(i=0;i<num;i++){
		for(j=0;j<matches[i].n_num;j++){
			if(strcmp(matches[i].nodes[j].name, "NULL") == 0){
				nv = find_num_nodes(g,matches[i].nodes[j].varname);
				var = return_var(g,matches[i].nodes[j].varname);
				if (count == 0){
					ms = malloc(sizeof(match)*nv);
				}
				else{
					ms = realloc(ms,sizeof(match)*(nv+count));
				}
				for(k=0;k<nv;k++){
					ms[count] = create_m_copy(matches[i]);
					strcpy(ms[count].nodes[j].name,var->nodes[k].name);
					
					count++;
				}
			}
		}
		
	}
	if(count == 0){
		matchs* matchess = malloc(sizeof(matchs));
		matchess->num = num;
		matchess->matches = matches;
		for(i=0;i<matchess->num;i++)
			replacene(matchess->matches[i].nodes,matchess->matches[i].edges, matchess->matches[i].n_num, matchess->matches[i].e_num);
		rm_wr_matches(matchess, g);
		return matchess;
	}
	else
		return find_all_possible_matches(g,ms,count);
}

void printvariables(graph* g){
	int i,j;
	variable* tv;
	node* tn;
	for(i=0;i<g->v_num;i++){
		tv = &(g->variables[i]); 
		printf("%s\n",tv->name);
		
		for(j=0;j<tv->num;j++){
			tn = &(tv->nodes[j]);
			printf("%s\n",tn->name);
		}
	}
}

void printmatchs(matchs* ms){
	int i;
	for(i=0;i<ms->num;i++){
		printf("match %d\n",i);
		printmatch(&(ms->matches[i]));
	}
}


void printmatch(match* m){
	printf("Nodes\n");
	int i;
	for(i=0;i<m->n_num;i++)
		printf("%s\n",m->nodes[i].name);
	printf("Edges\n");
	for(i=0;i<m->e_num;i++)
		printf("(%s,%s,%s)\n",m->edges[i].node1.name,m->edges[i].attribute,m->edges[i].node2.name);
}

void printnodes(graph* g){
	int i;
	for(i=0;i<g->n_num;i++)
		printf("%s\n",g->nodes[i].name);
}

void printedges(graph* g){
	int i;
	for(i=0;i<g->e_num;i++)
		printf("(%s,%s,%s)\n",g->edges[i].node1.name,g->edges[i].attribute,g->edges[i].node2.name);
}

int exists(node* ns, node n,int num){
	int i;
	for(i=0;i<num;i++){
		if(strcmp(ns[i].name,n.name) == 0)
			return ns[i].flag;
	}
	return -1;
}

void cull_ms(matchs* ms,node n){
	int i;
	for(i=0;i<ms->num;i++){
		if(exists(ms->matches[i].nodes,n,ms->matches[i].n_num) == 1)
			m_rb(ms,i);
	}	
}

int rm_m(edge* edges,graph* g,match m){
	int i,j,count=0,num,flag = 0;
	edges = malloc(sizeof(edge)*g->e_num);
	for(i=0;i<g->e_num;i++){
		if(m->e_num > m->n_num)
			num = m->e_num;
		else
			num = m->n_num;
		for(j=0;j<num;j++){
			if((num<n_num) && (flag == 0)){
				if((strcmp(g->edges[i].node1.name,m.nodes[j].name) == 0) || (strcmp(g->edges[i].node1.name,m.nodes[j].name) == 0) ) 
					flag = 1;
			}
			if(num<e_num){
				if (e_cmp(m.edges[j],g->edges[i]) == 0){
					flag = 0;
					break;
				}
			}
		}
		if(flag == 1){
			copy_edge(&edges[count],&g->edges[i]);
			count++;
		}
		flag =0;
	}
	return count;
	/*for(i=0;i<m->e_num;i++){
		for(j=0;j<g->e_mum;j++){
			if(e_cmp(m.edges[i],g->edges[j]) == 0){
				
				break;
			}
			
		}
	}*/
}

int ret_inc(graph* p,char* varname){
	int i,count;
	for(i=0;i<p->n_num;i++){
		if(strcomp(varname,p->nodes[i].varname)==0)
			count=p->nodes[i].flag;
	}
	return (count+1);
}

graph create_p_copy(graph p){
	graph c;
	c.nodes = create_ns_copy(p.nodes,p.n_num);
	c.n_num = p.n_num;
	c.edges = create_es_copy(p.edges,p.e_num);
	c.e_num = p.e_num;
	return c;
}

void rm_dups(context* con){
	int i,j,g;
	for(i=0;i<con->num;i++){
		for(j=i+1;j<con->num;j++){
			if(strcmp(con->nodes[i].name,con->nodes[i].name)==0){
				for(g=j;f<con->num-1;g++){
					swap(&con->nodes[g],&con->nodes[g+1]);
				}
			}
		}
	}
}

void context_gen(matchs* ms,context* cons,char* varname,int num){
	cons = malloc(sizeof(cons)*num);
	int i,j;
	for(i=0;i<num;i++){
		cons[i].nodes = malloc(sizeof(node)*ms->num);
		cons[i].num =0;
	}
	for(i=0;i<ms->num;i++){
		for(j=0;j<ms->matches[i].n_num;j++){
			if(strcmp(ms->matches[i].nodes[j].varname,varname) == 0 && ms->matches[i].nodes[j].flag >0){
				copy_node(&cons[ms->matches[i].nodes[j].flag].nodes[cons[i].num],&ms->matches[i].nodes[j]);
				cons[i].num++;
			}
		}
	}
	for(i=0;i<num;i++){
		rm_dups(&cons[i]);	
	}
}

int find_pattern_children(graph* children,graph* g, graph* p,matchs* matches,node n){
	cull_ms(matches,n);
	int i,j,num,count=0,f,f2,t;
	edge* edges;
	for(i=0;i<matches->num;i++){
		num = rm_m(edges,g, matches[i]);
		children = malloc(sizeof(graph)*num*2);
		for(j=0;j<num;j++){
			f = exists(matches->matches[i].nodes,edges[j].node1,matches->matches[i].n_num);
			f2 = exists(matches->matches[i].nodes,edges[j].node2,matches->matches[i].n_num);
			if( f != -1 ){
				if( f2 != -1){
					children[count] = create_p_copy(*p);
					t = children[count].e_num;
					children[count].e_num++;
					realloc(children[count].edges,sizeof(edge)*(t+1));
					strcpy(children[count].edges[t].node1.name,"NULL");
					strcpy(children[count].edges[t].node1.varname,edges[j].node1.varname);
					children[count].edges[t].node1.flag = f;
					strcpy(children[count].edges[t].node2.name,"NULL");
					strcpy(children[count].edges[t].node2.varname,edges[j].node2.varname);
					children[count].edges[t].node1.flag = f2;
					count++;
				}
				else{
					children[count] = create_p_copy(*p);
					t = children[count].n_num;
					realloc(children[count].nodes,sizeof(node)*(t+2));
					children[count].n_num = children[count].n_num + 2;
					strcpy(children[count].nodes[t].name,"NULL");
					strcpy(children[count].nodes[t].varname,edges[j].node2.varname);
					children[count].nodes[t].flag = ret_inc(p,edges[j].node2.varname);
					t++;
					copy_node(&children[count].nodes[t],&edges[j].node2);
					t = children[count].e_num;
					children[count].e_num = children[count].e_num + 2;
					realloc(children[count].edges,sizeof(edge)*(t+2));
					strcpy(children[count].edges[t].node1.name,"NULL");
					strcpy(children[count].edges[t].node1.varname,edges[j].node1.varname);
					children[count].edges[t].node1.flag = f;
					copy_node(&children[count].edges[t].node2,&edges[j].node2);
					t++;
					strcpy(children[count].edges[t].node1.name,"NULL");
					strcpy(children[count].edges[t].node1.varname,edges[j].node1.varname);
					children[count].edges[t].node1.flag = f;
					strcpy(children[count].edges[t].node2.name,"NULL");
					strcpy(children[count].edges[t].node2.varname,edges[j].node2.varname);
					children[count].edges[t].node2.flag = ret_inc(p,edges[j].node2.varname);
					count++;
				}
			}
			else{
				children[count] = create_p_copy(*p);
				t = children[count].n_num;
				realloc(children[count].nodes,sizeof(node)*(t+2));
				children[count].n_num = children[count].n_num + 2;
				strcpy(children[count].nodes[t].name,"NULL");
				strcpy(children[count].nodes[t].varname,edges[j].node1.varname);
				children[count].nodes[t].flag = ret_inc(p,edges[j].node1.varname);
				t++;
				copy_node(&children[count].nodes[t],&edges[j].node1);
				t = children[count].e_num;
				children[count].e_num = children[count].e_num + 2;
				realloc(children[count].edges,sizeof(edge)*(t+2));
				strcpy(children[count].edges[t].node2.name,"NULL");
				strcpy(children[count].edges[t].node2.varname,edges[j].node2.varname);
				children[count].edges[t].node2.flag = f2;
				copy_node(&children[count].edges[t].node1,&edges[j].node1);
				t++;
				strcpy(children[count].edges[t].node1.name,"NULL");
				strcpy(children[count].edges[t].node1.varname,edges[j].node1.varname);
				children[count].edges[t].node1.flag = ret_inc(p,edges[j].node1.varname);
				strcpy(children[count].edges[t].node2.name,"NULL");
				strcpy(children[count].edges[t].node2.varname,edges[j].node2.varname);
				children[count].edges[t].node2.flag = f2;
				count++;
			}	
		}
	}
	return count;
}



/*int sf(node n,attributes ats,context c,graph* g){
	int i;
	
}*/
