#ifndef VARIABLE_T
#define VARIABLE_T
#include "node.h"
#include "list.h"

typedef struct variable{
	char name[100];
	void* list;
	int flag;
	struct variable* next;
}variable;


typedef struct vlist{
	variable* head;
	variable* tail;
	int num;
}vlist;

vlist* create_vlist();

variable* create_variable(char* name,int flag);

variable* add_variable(vlist* list,variable* t);

variable* add_variable_m(vlist* list,char* name,int flag);

void delete_vlist(vlist* list);

variable* find_v_byname(vlist* list,char* name);

variable* find_v_byplace(vlist* list,int num);

void print_variable(variable* t);

void print_vlist(vlist* list);
#endif
