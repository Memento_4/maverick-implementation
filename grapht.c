#include "grapht.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

graph* create_graph(){
	graph* t = malloc(sizeof(graph));
	t->nl = create_nlist();
	t->el = create_elist();
	t->vl = create_vlist();
	return t;
}

node* add_node_g(graph* g,char* name){
	return add_node_m(g->nl,name);
}

node* add_node_vg(graph* g,char* name,char* varname){
	return add_node_vm(g->nl,name,varname,g->vl);
}

edge* add_edge_g(graph* g,char* from,char* to,char* attrname){
	return add_edge_m(g->el,from,to,attrname,g->nl);
}

variable* add_variable_g(graph* g,char* name,int flag){
	return add_variable_m(g->vl,name,flag);
}

void delete_graph(graph* g){
	delete_elist(g->el);
	delete_nlist(g->nl);
	delete_vlist(g->vl);
	free(g);
}


void print_graph(graph* g){
	print_vlist(g->vl);
	print_nlist(g->nl);
	print_elist(g->el);
}


graph* read_graph(char* file){
	graph* g = create_graph();
	FILE* fp;
	fp = fopen(file,"r");
	char name[100],varname[100],name2[100];
	int i,j,numv,numa,nume,numn;
	fscanf(fp,"%d %d %d",&numv,&numa,&nume);
	for(i=0;i<numv;i++){
		fscanf(fp,"%s %d",varname,&numn);
		add_variable_g(g,varname,0);
		for(j=0;j<numn;j++){
			fscanf(fp,"%s",name);
			add_node_vg(g,name,varname);
		}
	}
	for(i=0;i<nume;i++){
		fscanf(fp,"%s %s %s",name,varname,name2);
		add_edge_g(g,name,name2,varname);
	}
	fclose(fp);
	return g;
}

graph* copy_graph(graph* g){
	graph* t = create_graph();
	int i;
	node* n;
	variable* v;
	edge* e;
	v = g->vl->head;
	for(i=0;i<g->vl->num;i++){
		add_variable_g(t,v->name,v->flag);
		v = v->next;
	}
	n = g->nl->head;
	for(i=0;i<g->nl->num;i++){
		v = n->variable;
		add_node_vg(t,n->name,v->name);
		n = n->next;
	}
	e = g->el->head;
	for(i=0;i<g->el->num;i++){
		node* n2 = e->to;
		n = e->from;
		add_edge_g(t,n->name,n2->name,e->name);
		e = e->next;
	}
	return t;
}


int is_good_match(graph* g,graph* m){
	if(has_dup_n(m->nl) == 1)
		return 0;
	else if(proper_es(m->el,g->el) == 0)
		return 0;
	return 1;
}

void delete_bad_matches(list* matches,graph* g){
	int i,num = matches->num;
	member* m = matches->head;
	member* t;
	graph* match = m->node;
	for(i=0;i<num;i++){
		if(is_good_match(g,match) == 0){
			t = m;
			m = m->next;
			delete_member_g(matches,t);
		}
		else
			m = m->next;
		match = m->node;
	}
}

list* match(graph* g, graph* p){
	list* matches = create_list();
	add_member(matches,copy_graph(p));
	return find_matches(g,matches,1);
}

list* find_matches(graph* g,list* matches,int num){//remove unneeded code and printfs
	int i,j,k,count=0;
	for(i=0;i<num;i++){
		//printf("num is %d\n",i);
		graph* m = find_m_bynum(matches,i);
		nlist* nl = m->nl;
		node* n= nl->head;
		for(j=0;j<nl->num;j++){
			if(n->name[0] == '?'){
				variable* v = n->variable;
				v = find_v_byname(g->vl,v->name);
				list* l = v->list;
				int pv = l->num;
				member* me = l->head;
				//printf("head is %s\n",((node*)me->node)->name);
				for(k=0;k<pv;k++){
					//graph* t = (add_member(matches,copy_graph(m)))->node;
					graph* t = copy_graph(m);
					//printf("%s\n",((variable*)((node*)t->el->head->to)->variable)->name);
					node* tn = find_n_byplace(t->nl,j);
					strcpy(tn->name,((node*)me->node)->name);
					if(is_good_match(g,t) == 0){
						delete_graph(t);
						me = me->next;
						continue;
					}
					else
						add_member(matches,t);
					//printf("checking... %s\n",tn->name);
					me = me->next;
					count++;
				}
				break;
			}
			n = n->next;
		}
	}
	//printf("count is %d \n",count);
	if(count ==0){
		//print_list_g(matches);
		delete_bad_matches(matches,g);
		return matches;
	}
	else{
		delete_n_members_g(matches,num);
		return find_matches(g,matches,count);
	}
}


/*
list* find_matches(graph* g,list* matches,int num){//remove unneeded code and printfs
	int i,j,k,count=0;
	for(i=0;i<num;i++){
		graph* m = find_m_bynum(matches,i);
		nlist* nl = m->nl;
		node* n= nl->head;
		for(j=0;j<nl->num;j++){
			if(n->name[0] == '?'){
				variable* v = n->variable;
				v = find_v_byname(g->vl,v->name);
				list* l = v->list;
				int pv = l->num;
				member* me = l->head;
				//printf("head is %s\n",((node*)me->node)->name);
				for(k=0;k<pv;k++){
					graph* t = (add_member(matches,copy_graph(m)))->node;
					//printf("%s\n",((variable*)((node*)t->el->head->to)->variable)->name);
					node* tn = find_n_byplace(t->nl,j);
					strcpy(tn->name,((node*)me->node)->name);
					//printf("checking... %s\n",tn->name);
					me = me->next;
					count++;
				}
				break;
			}
			n = n->next;
		}
	}
	if(count ==0){
		//print_list_g(matches);
		delete_bad_matches(matches,g);
		return matches;
	}
	else{
		delete_n_members_g(matches,num);
		return find_matches(g,matches,count);
	}
}*/

void delete_n_members_g(list* list,int num){
	graph* g;
	member* m;
	m = list->head;
	g = m->node;
	int i;
	for(i=0;i<num;i++){
		list->head = m->next;
		list->tail->next = m->next;
		delete_graph(g);
		free(m);
		list->num--;
		m = list->head;
		g = m->node;
	}
	if(list->num == 0){
		list->head = NULL;
		list->tail = NULL;
	}
}

void delete_member_g(list* list,member* mem){
	int i;
	member *t= list->head;
	member *tn= list->tail;
	for(i=0;i<list->num;i++){
		if(t == mem){
			if(i==0){
				list->head = t->next;
				list->tail->next = t->next;
				delete_graph(mem->node);
				free(mem);
			}
			else{
				tn->next = t->next;
				if(i == list->num-1)
					list->tail = tn;
				delete_graph(mem->node);
				free(mem);
			}
			break;
		}
		t = t->next;
		tn = tn->next;
	}
	list->num--;
}



void print_list_g(list* list){
	int i;
	member* t = list->head;
	for(i=0;i<list->num;i++){
		print_graph(t->node);
		t = t->next;
	}
}


void delete_list_g(list* list){
	int i,num = list->num;
	member* t = list->head;
	for(i=0;i<num;i++){
		list->head = t->next;
		delete_graph(t->node);
		free(t);
		t = list->head;
	}
	free(list);
}


int find_next_qm(graph* p){
	int i,count = 0;
	node* n = p->nl->head;
	for(i=0;i<p->nl->num;i++){
		if(n->name[0] == '?')
			count++;
		n = n->next;		
	}
	count++;
	return count;
}
