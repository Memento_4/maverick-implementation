#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grapht.h"
#include "context.h"

int main(int argc, char *argv[]) {
	/*graph* g = create_graph();
	char name[100];
	char name2[100];
	int i;
	//printf("guard 0\n");
	for(i=0;i<3;i++){
		sprintf(name,"node%d",i);
		add_node_g(g,name);
	}
	//printf("guard 1\n");;
	for(i=0;i<2;i++){
		sprintf(name,"node%d",i);
		sprintf(name2,"node%d",i+1);
		add_edge_g(g,name,name2,"test");
	//	printf("guard 1.5\n");
	}
	//printf("guard 2\n");
	print_graph(g);
	print_node_edges(g->nl->tail);
	delete_graph(g);*/
	graph* g = read_graph("skg");
	graph* p = read_graph("patern2");
	/*edge* e = p->el->head;
	p->el->head->to = p->nl->tail;
	printf("hi\n");
	printf("%s\n",((variable*)((node*)p->el->head->from)->variable)->name);
	printf("%s\n",((variable*)((node*)p->el->head->to)->variable)->name);*/
	//print_graph(g);
	list* matches = match(g,p);
	//print_graph(g);
	print_list_g(matches);
	printf("\n\n");
	list* contexts = find_contexts(g,p,matches,"CRO");
	print_contexts(contexts);
	delete_list_list(contexts);
	delete_graph(g);
	delete_graph(p);
	delete_list_g(matches);
}
