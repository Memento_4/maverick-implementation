# Maverick Implementation

An implementation of the Maverick framework in c

By changing the define values in mainf.c file the beam width, max number of iterations, number of best results wanted and the entity of interest can be selected.

mainf.c is designed to take in the articles_kg.tsv file as it's base for the knowledge graph which it's build using the Source Name and Target Name collumns as nodes while the Event Text collumn is used to provide the attributes between the nodes.

In other words every line of the articles_kg.tsv file provides a new edge. How many lines will be used can be adjusted by changing the value of 
NLINES.

Currently the program is too slow taking too long to complete more than one iterations with 100 edges.


Bellow is a summary of how each part of the Maverick Framework is implemanted.

Context-Evaluator: The context evaluator first generates knowledge graphs in the image of the pattern by changing all the variables in it to their possible values. Then it removes the matches that aren't a subgraph of our knowledge graph.

The contexts are generated using the matches for every different variable in the pattern a context is defined by all the different vallues it has in the matches generated.

Exceptionality-evaluator: It uses the one-out-of-few scoring function and it's working is mostly as seen in the pseudo code.

Pattern-Generator: The pattern generator is also also mostly see as in the pseudo code. The pattern-generator is suspected to be a large reason why a second iteration can be so slow as the heuristic to decide which children patterns are the most promising requires the generation of matches and contexts for every child which is indeed counter productive.
