#ifndef NODE_H
#define NODE_H
#include <stdbool.h>
#include "edge.h"
#include "list.h"
#include "variable.h"

typedef struct node{
	char name[100];
	void* list;
	void* variable;
	struct node* next;
}node;

typedef struct nlist{
	node* head;
	node* tail;
	int num;
}nlist;


nlist* create_nlist();

node* create_node(char* name);

node* create_node_v(char* name,char* varname,void* list);

node* add_node(nlist* list,node* t);

node* add_node_m(nlist* list,char* name);

node* add_node_vm(nlist* list,char* name,char* varname,void* lis);

void delete_nlist(nlist* list);

node* find_n_byname(nlist* list,char* name);

node* find_n_byplace(nlist* list,int num);

int find_pn_byname(nlist* list,char* name);

void print_node(node* n);

void print_nlist(nlist* list);

void print_node_edges(node* n);

void print_node_ledges(nlist* list);

int has_dup_n(nlist* list);

//void print_list_n(list* list);

#endif
