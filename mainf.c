#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "grapht.h"
#include "context.h"
#include "pattern.h"

#define eoi "North Atlantic Treaty Organization"
#define k 5
#define w 3
#define inputf "skg"
#define MAX_ITTERATION 4
#define NLINES 1000

graph* starting_pattern(graph* g,char* interest);

void copy_to_main(list* mpairs,list* pairs);

void copy_to_main_patterns(list* mpairs,list* pairs);

void copy_patterns_without(list* patterns,list* pss);

void delete_contextss(list* contextss);

void delete_used_contexts(list* pairs,list* contexts);

void delete_zeroscore_pairs(list* pairs);

graph* read_from_tsv(char* name);

char** get_elements(char* token);

int main(int argc, char *argv[]) {
	char out[100];
	sprintf(out,"results/%s.txt",eoi);
	freopen(out,"a+", stdout);
	clock_t tim;
	tim = clock();
	graph* g= read_from_tsv("articles_kg.tsv");
	tim = clock() -  tim;
	double ttk = ((double)tim)/CLOCKS_PER_SEC;
	//read_from_tsv("articles_kg.tsv");
	printf("graph has been created in %f seconds\n",ttk);
	//print_graph(g);
	//graph* g = read_graph("skg");
	graph* p = starting_pattern(g,eoi);
	int i = 1;
	list* contexts;
	list* matches;
	list* patterns = create_list();
	add_member(patterns,p);
	list* mpairs = create_list();
	list* contextss = create_list();
	double ttm=0;
	double ttc=0;
	double ttev=0;
	double ttp=0;
	while(patterns->num !=0 && i<MAX_ITTERATION){
		//printf("ITTERATION IS %d\n",i);
		list* tps = create_list();
		int j;
		member* m = patterns->head;
		for(j=0;j<patterns->num;j++){
			tim = clock();
			matches = match(g,m->node);
			tim = clock() - tim;
			ttk = ((double)tim)/CLOCKS_PER_SEC;
			ttm += ttk;
			//printf("matches for pattern number %d finished in %f seconds\n",j,ttk);
			tim = clock();
			contexts = find_contexts(g,m->node,matches,eoi);
			tim = clock() - tim;
			ttk = ((double)tim)/CLOCKS_PER_SEC;
			ttc += ttk;
			//printf("contexts for pattern number %d finished in %f seconds\n",j,ttk);
			delete_used_contexts(mpairs,contexts);//CHANGE
			add_member(contextss,contexts);
			int h;
			member* mc = contexts->head;
			tim = clock();
			for(h=0;h<contexts->num;h++){
				list* tpairs = exceptionality_evaluator(eoi,mc->node,k,g->nl);
				copy_to_main(mpairs,tpairs);
				mc = mc->next;
			}
			tim = clock() - tim;
			ttk = ((double)tim)/CLOCKS_PER_SEC;
			ttev += ttk;
			//printf("exceptionality evaluator for contexts from pattern number %d finished in %f seconds\n",j,ttk);
			//delete_list_list(contexts);
			tim = clock();
			list* cps = pattern_generator(eoi,m->node,matches,w,g);
			tim = clock() - tim;
			ttk = ((double)tim)/CLOCKS_PER_SEC;
			ttp += ttk;
			//printf("patern generator for pattern number %d finished in %f seconds\n",j,ttk);
			delete_list_g(matches);
			copy_to_main_patterns(tps,cps);
			m = m->next;
		}
		delete_list_g(patterns);
		while(tps->num > w)
			delete_last_pattern_score(tps);
		patterns = create_list();
		copy_patterns_without(patterns,tps);
		i++;
	}
	delete_list_g(patterns);
	delete_zeroscore_pairs(mpairs);
	while(mpairs->num > k)
		delete_last_pair(mpairs);
	printf("PRINTING BEST PAIRS FOR ENTITY %s\n",eoi);
	node* n = find_n_byname(g->nl,eoi);
	print_pairs_n(mpairs,eoi,n->list);
	//print_pairs(mpairs);
	printf("Total time spent finding matches (excluding those used for children evaluation) %f seconds\n",ttm);
	printf("Total time spent finding contexts %f seconds\n",ttc);
	printf("Total time spent evaluating contexts %f seconds\n",ttev);
	printf("Total time spent generating patterns %f seconds\n",ttp);
	fclose(stdout);
	delete_pairs(mpairs);
	delete_contextss(contextss);//CHANGE
	//printf("was out\n");
	delete_graph(g);
}

graph* starting_pattern(graph* g,char* interest){
	graph* t = create_graph();
	int i;
	node* n = find_n_byname(g->nl,interest);
	variable* v;
	v = g->vl->head;
	for(i=0;i<g->vl->num;i++){
		add_variable_g(t,v->name,v->flag);
		v = v->next;
	}
	v = n->variable;
	add_node_vg(t,"?1",v->name);
	return t;
}

void copy_to_main(list* mpairs,list* pairs){
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num;i++){
		add_member_sort_pair(mpairs,m->node);
		m = m->next;
	}
	delete_list(pairs);
}

void copy_to_main_patterns(list* mpairs,list* pairs){
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num;i++){
		add_member_sort_ps(mpairs,m->node);
		m = m->next;
	}
	delete_list(pairs);
}

void copy_patterns_without(list* patterns,list* pss){
	int i;
	member* m = pss->head;
	for(i=0;i<pss->num;i++){
		pattern_score* tps = m->node;
		add_member(patterns,tps->pattern);
		free(tps);
		member* t = m;
		m = m->next;
		free(t);
	}
	free(pss);
	//delete_list(pss);
}

void delete_contextss(list* contextss){
	int i;
	member* m = contextss->head;
	for(i=0;i<contextss->num;i++){
		//delete_list_list(m->node);
		delete_contexts(m->node);
		member* t = m;
		m = m->next;
		free(t);
	}
	free(contextss);
}


void delete_used_contexts(list* pairs,list* contexts){
	int i,j;
	member* m = pairs->head;
	for(i=0;i<pairs->num;i++){
		pair* p = m->node;
		int num = contexts->num;
		member* c = contexts->head;
		context* cont = p->cont;
		for(j=0;j<num;j++){
			context* contc = c->node;
			if(isa_duplicate(cont->nodes,contc->nodes) == true){
				member* t = c->next;
				delete_member_c(contexts,c);
				c = t;
			}
			else
				c = c->next;
		}
		m = m->next;
	}
}

void delete_zeroscore_pairs(list* pairs){
	if(pairs->num == 0)
		return;
	int i,j;
	pair* p = pairs->tail->node;
	while(p->score == 0){
		delete_last_pair(pairs);
		if(pairs->num ==0)
			return;
		p = pairs->tail->node;
	}
}


graph* read_from_tsv(char* name){
	FILE* fp;
	int i;
	//i == 5
	graph* g = create_graph();
	fp = fopen(name,"rb");
	char buffer[1000];
	char var[2][100];
	char* token;
	fgets(buffer,1000,fp);
	token = strtok(buffer, "	");
	i=0;
	while(token != NULL){
		token = strtok(NULL,"	");
		i++;
		if(i==2){
			strcpy(var[0],token);
			add_variable_g(g,var[0],0);
		}
		if(i==8){
			strcpy(var[1],token);
			add_variable_g(g,var[1],0);
			break;
		}
	}
	int c =0;
	while(fgets(buffer,1000,fp)!= NULL){
		i=0;

		char** element = get_elements(buffer);
		if(find_n_byname(g->nl,element[0]) == NULL){
			add_node_vg(g,element[0],var[0]);
		}
		if(find_n_byname(g->nl,element[2]) == NULL){
			add_node_vg(g,element[2],var[1]);
		}
		add_edge_g(g,element[0],element[2],element[1]);
		c++;
		
		free(element[1]);
		free(element[0]);
		free(element[2]);
		free(element);
		if(c==NLINES)
			break;
	}
	fclose(fp);
	return g;	
}

char** get_elements(char* token){
	int i=0;
	int j;
	char** element;
	element = malloc(sizeof(char*)*3);
	for(j=0;j<3;j++){
		element[j] = malloc(sizeof(char)*100);
	}
	j=0;
	int c=0;
	int g=0;
	while(i!=9){
		if(i == 2 || i == 5 || i == 8){
			if(token[j] == '	'){
				element[g][c] = '\0';
				c=0;
				g++;
			}
			else{
				element[g][c] = token[j];
				c++;
			}
		}
		if(token[j] == '	')
			i++;
		j++;
	}
	return element;
}
