#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "context.h"

typedef struct maxout{
	attribute* pattr;
	float uppermax;
}maxout;

char* turn_ptos(graph* p){
	char* st = malloc(1000);
	char* t;
	st[0] = '\0';
	elist* ell = p->el;
	edge* e = ell->head;
	int i;
	//printf("number of edges is %d\n",ell->num);
	for(i=0;i<ell->num;i++){
		t =turn_etos(e);
		strcat(st,t);
		free(t);
		e = e->next;
	}
	return st;
}

list* find_contexts(graph* g,graph* p,list* matches,char* interest){
	int i;
	list* contexts = create_list();
	nlist* nl = p->nl;
	node* n = nl->head;
	char* st = turn_ptos(p);
	//printf("printing pattern %s\n",st);
	for(i=0;i<nl->num;i++){
		if(n->name[0] == '?'){
			list* contn = create_list();
			int j;
			member* m = matches->head;
			for(j=0;j<matches->num;j++){
				graph* match = m->node;
				node* t = find_n_byplace(match->nl,i);
				if(exists(t->name,contn) == 0){
					//printf("%s\n",t->name);
					node* in = find_n_byname(g->nl,t->name);
					add_member(contn,in);
				}
				m = m->next;
			}
			if(exists(interest,contn)==1){
				context* cont = malloc(sizeof(context));
				strcpy(cont->pat,st);
				char* at = (n->name)+1;
				cont->tvar = atoi(at);
				cont->nodes = contn;
				add_member(contexts,cont);
			}
			else
				delete_list(contn);//CHANGE
		}
		n = n->next;
	}
	cull_duplicate_contexts(contexts);//CHANGE
	free(st);
	return contexts;
}

bool isa_duplicate(list* cont1,list* cont2){
	int i;
	member* m = cont1->head;
	if(cont1->num != cont2->num)
		return false;
	for(i=0;i<cont1->num;i++){
		node* n = m->node;
		if(exists(n->name,cont2) == 0)
			return false;
		m = m->next;
	}
	return true;
}

void delete_member_c(list* list,member* mem){
	int i;
	member *t= list->head;
	member *tn= list->tail;
	for(i=0;i<list->num;i++){
		if(t == mem){
			if(i==0){
				list->head = t->next;
				list->tail->next = t->next;
				context* cont = mem->node;
				delete_list(cont->nodes);
				free(cont);
				free(mem);
			}
			else if(i==list->num-1){
				list->tail = tn;
				list->tail->next = list->head;
				context* cont = mem->node;
				delete_list(cont->nodes);
				//delete_list(mem->node);
				free(cont);
				free(mem);
			}
			else{
				tn->next = t->next;
				context* cont = mem->node;
				delete_list(cont->nodes);
				//delete_list(mem->node);
				free(cont);
				free(mem);
			}
			break;
		}
		t = t->next;
		tn = tn->next;
	}
	list->num--;
}


void cull_duplicate_contexts(list* contexts){
	int i,j;
	member* m1 = contexts->head;
	for(i=0;i<contexts->num-1;i++){
		context* con1 = m1->node;//change
		int num = contexts->num-i;
		member* m2 = m1->next;
		list* cont1 = con1->nodes;
		for(j=1;j<num;j++){
			context* con2 = m2->node;
			list* cont2 = con2->nodes;
			if( con2->tvar == con1->tvar && isa_duplicate(cont1,cont2) == true){
				member* t = m2->next;
				delete_member_c(contexts,m2);
				m2 = t;
			}
			else
				m2 = m2->next;
		}
		m1 = m1->next;
	}
}

int exists(char* name,list* context){
	int i;
	member* m = context->head;
	for(i=0;i<context->num;i++){
		node* n = m->node;
		if(strcmp(n->name,name) == 0)
			return 1;
		m = m->next;
	}
	return 0;
}

void delete_list_list(list* lis){
	int i;
	member* m = lis->head;
	for(i=0;i<lis->num;i++){
		list* l = m->node;
		delete_list(l);
		m = m->next;
	}
	delete_list(lis);
}

void print_context(context* cont){
	int i;
	list* conts = cont->nodes;
	member* m = conts->head;
	printf("Context has %d members\n",conts->num);
	printf("They result from pattern %s\n",cont->pat);
	printf("Using Variable %d\n",cont->tvar);
	printf("They are {\n");
	for(i=0;i<conts->num;i++){
		node* n = m->node;
		//printf("hey it's in context\n");
		printf("%s\n",n->name);
		m = m->next;
	}
	printf("}\n");
}

void print_contexts(list* lis){
	int i;
	member* m = lis->head;
	for(i=0;i<lis->num;i++){
		context* l = m->node;
		print_context(l);
		m = m->next;
	}
}

projection* create_projection(){
	projection* p =malloc(sizeof(projection));
	p->nodes = create_list();
	p->pjs = create_list();
	p->score = 0;
	return p;
}


void find_poa(node* n,attribute* attr,projection* p){
	int i,j;
	list* edges = n->list;
	member* edg = edges->head;
	int flag = 0;
	for(i=0;i<edges->num;i++){
		edge* e = edg->node;
		if(strcmp(attr->name,e->name) == 0){
			node* t;
			if(attr->direction == 0){
				t = e->from;
			}
			else{
				t = e->to;
			}
			member* m = p->pjs->head;
			for(j=0;j<p->pjs->num;j++){
				if(strcmp( ((node*)m->node)->name , t->name) == 0){
					flag = 1;
					break;
				}
				m = m->next;
			}
			if(flag == 0)
				add_member(p->pjs,t);
			flag =0;
		}
		edg = edg->next;
	}
}

projection* find_projection(node* n,list* attrs){
	projection* p = create_projection();
	int i;
	member* atr = attrs->head;
	for(i=0;i<attrs->num;i++){
		attribute* at = atr->node;
		find_poa(n,at,p);
		atr = atr->next;
	}
	add_member(p->nodes,n);
	return p;
}


bool pjs_cmp(list* p1,list* p2){
	int i;
	member* m = p1->head;
	for(i=0;i<p1->num;i++){
		char* name = ((node*)m->node)->name;
		if(exists(name,p2) == 0)
			return false;
		m = m->next;
	}
	return true;
}

projection* find_pr_inprs(list* projections, list* pjs){
	int i;
	member* m = projections->head;
	for(i=0;i<projections->num;i++){
		projection* p = m->node;
		if(pjs_cmp(p->pjs,pjs) == true)
			return p;
		m = m->next;
	}
	return NULL;
}

void delete_projection(projection* proj){
	delete_list(proj->nodes);
	delete_list(proj->pjs);
	free(proj);
}

void delete_projections(list* projections){
	int i;
	member* m = projections->head;
	for(i=0;i<projections->num;i++){
		delete_projection(m->node);
		m = m->next;
	}
	delete_list(projections);
}

list* find_projections(list* context,list* attrs){
	list* projections = create_list();
	projection* p;
	int i;
	member* m = context->head;
	for(i=0;i<context->num;i++){
		node* n = m->node;
		p = find_projection(n,attrs);
		projection* tp;
		//printf("yo yo\n");
		tp = find_pr_inprs(projections,p->pjs);
		if(tp == NULL)
			add_member(projections,p);
		else{
			add_member(tp->nodes,p->nodes->head->node);
		//	printf("wassup\n");
			delete_projection(p);
		//	printf("am out\n");
		}
		m = m->next;
	}
	return projections;
}

float calc_prob(list* projections,int num,char* interest){
	int i;
	member* m = projections->head;
	float t;
	for(i=0;i<projections->num;i++){
		projection* p = m->node;
		p->score = ((float) p->nodes->num)/num;
		if(exists(interest,p->nodes) == 1)
			t = p->score; 
		m = m->next;
	}
	return t;
}


float scf(list* context,list* subspace,char* interest){
	int i,j,count=0;
	list* projections = find_projections(context,subspace);
	float t = calc_prob(projections,context->num,interest);
	member* m = projections->head;
	for(i=0;i<projections->num;i++){
		projection* p = m->node;
		if(p->score > t)
			count = count + p->nodes->num;
		m = m->next;
	}
	delete_projections(projections);
	t = ((float) count)/context->num;
	return t;
}

bool exists_at(char* name,list* attrs){
	int i;
	member* m = attrs->head;
	for(i=0;i<attrs->num;i++){
		attribute* attr = m->node;
		if(strcmp(attr->name,name) == 0)
			return true;
		m = m->next;
	}
	return false;
}

list* find_attrs(node* n){
	list* attrs = create_list();
	list* edgs = n->list;
	int i;
	member* m = edgs->head;
	for(i=0;i<edgs->num;i++){
		edge* edg = m->node;
		if(exists_at(edg->name,attrs) == false){
			attribute* attr = malloc(sizeof(attribute));
			strcpy(attr->name,edg->name);
			if(n == edg->from)
				attr->direction = 1;
			else
				attr->direction = 0;
			add_member(attrs,attr);
		}
		m = m->next;
	}
	return attrs;
}

float upper_func(list* projections,char* interest,int cnum){
	int i,count = 0;
	member* m = projections->head;
	for(i=0;i<projections->num;i++){
		projection* p = m->node;
		if(p->score > (((float)1)/cnum)){
			//printf("haaaaaalo\n");
			if(exists(interest,p->nodes) == 0)
				count = count + p->nodes->num;
			else
				count = count + p->nodes->num - 1;
		}
		m = m->next;
	}
	float t = ((float) count)/cnum;
	//printf("float is %f\n",t);
	return t;
}

void remove_last(list* l){
	if(l->num == 1){
		free(l->head);
		l->head = NULL;
		l->tail = NULL;
		l->num--;
		return;
	}
	int i;
	member* m = l->head;
	for(i=0;i<l->num-2;i++)//was num-1 before considered a mistake but algorithm was running properly will need new tests
		m = m->next;
	member* t = l->tail;
	m->next = l->head;
	l->tail = m;
	free(t);
	l->num--;
}

maxout max_attr(list* cattrs,list* attrs,char* interest, list* context){
	int i;
	member* m = attrs->head;
	float max=0;
	attribute* maxa = m->node;
	for(i=0;i<attrs->num;i++){
		attribute* attr = m->node;
		add_member(cattrs,attr);
		list* projections = find_projections(context,cattrs);
		calc_prob(projections,context->num,interest);
		//printf("getting in upper func\n");
		float t = upper_func(projections,interest,context->num);
		if(t > max){
			max = t;
			maxa = attr;
		}
		delete_projections(projections);
		remove_last(cattrs);
		m = m->next;
	}
	maxout mo;
	mo.pattr = maxa;
	mo.uppermax = max;
	return mo;
}

list* copy_subspace(list* sub){
	int i;
	list* copy = create_list();
	member* m = sub->head;
	for(i=0;i<sub->num;i++){
		attribute* t = m->node;
		attribute* atr = malloc(sizeof(attribute));
		strcpy(atr->name,t->name);
		atr->direction = t->direction;
		add_member(copy,atr);
		m = m->next;
	}
	return copy;
}

void add_member_sort_pair(list* pairs,pair* pa){
	member* m = malloc(sizeof(member));
	m->node = pa;
	if(pairs->num == 0){
		pairs->head = m;
		pairs->tail = m;
		m->next = m;
	}
	else if(pairs->num == 1){
		pair* p = pairs->head->node;
		if(p->score > pa->score){
			pairs->head->next = m;
			pairs->tail = m;
			m->next = pairs->head;
		}
		else{
			pairs->head->next = m;
			m->next = pairs->head;
			pairs->head = m;
			pairs->tail = m->next;
		}
	}
	else{
		pair* p = pairs->head->node;
		pair* lp = pairs->tail->node;
		if(p->score < pa->score){
			m->next = pairs->head;
			pairs->tail->next = m;
			pairs->head = m;
		}
		else if(lp->score > pa->score){
			m->next = pairs->head;
			pairs->tail->next = m;
			pairs->tail = m;
		}
		else{
			member* m1 = pairs->head;
			member* m2 = m1->next;
			int i;
			for(i=0;i<pairs->num-1;i++){
				pair* p1 = m1->node;
				pair* p2 = m2->node;
				if(p1->score >= pa->score && p2->score <= pa->score){
					m1->next = m;
					m->next = m2;
					break;
				}
				m1 = m1->next;
				m2 = m2->next;
			}
		}
	}	
	pairs->num++;
}

void delete_subspace(list* subspace){
	int i;
	member* m = subspace->head;
	for(i=0;i<subspace->num;i++){
		free(m->node);
		member* t = m;
		m = m->next;
		free(t);
	}
	free(subspace);
}

void delete_pair(pair* pa){
	delete_subspace(pa->subspace);
	free(pa);
}

void delete_last_pair(list* pairs){
	if(pairs->num == 1){
		delete_pair(pairs->head->node);
		free(pairs->head);
		pairs->num--;
		return;
	}
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num-2;i++)
		m = m->next;
	delete_pair(m->next->node);
	free(m->next);
	m->next = pairs->head;
	pairs->tail = m;
	pairs->num--;
}

list* explore_subspace(char* interest,context* cont,list* cattrs,list* attrs,list* pairs,int k){
	int i = 0;
	list* conts = cont->nodes;
	while(attrs->num !=0 ){
		//printf("%d\n",i);
		maxout mo = max_attr(cattrs,attrs,interest,conts);
		attribute* amax = mo.pattr;
		float upmax = mo.uppermax;
		add_member(cattrs,amax);
		delete_member_without(attrs,find_member(attrs,amax));
		attribute* amin;
		float scoremin;
		if(pairs->num < k){
			scoremin = -1;
			amin = NULL;
		}
		else{
			member* m = pairs->tail;
			pair* pa = m->node;
			scoremin = pa->score;
		}
		if(upmax > scoremin){
			float score = scf(conts,cattrs,interest);
			if(score > scoremin){
				if(scoremin >= 0){
					delete_last_pair(pairs);
				}
				pair* pa = malloc(sizeof(pair));
				
				//copy_subspace(pa->subspace,cattrs);
				pa->subspace = copy_subspace(cattrs);
				pa->cont = cont;
				pa->score = score;
				add_member_sort_pair(pairs,pa);
			}
			//explore_subspace(interest,context,cattrs,attrs,pairs,k);
		}
		i++;
	}
	return pairs;
}

void delete_list_attr(list* attrs){
	int i;
	member* m = attrs->head;
	for(i=0;i<attrs->num;i++){
		free(m->node);
		member* t = m;
		m = m->next;
		free(t);
	}
	free(attrs);
}

list* exceptionality_evaluator(char* interest,context* cont,int k,nlist* ns){//CHANGE
	list* cattrs = create_list();
	list* pairs = create_list();
	node* n = find_n_byname(ns,interest);
	list* attrs = find_attrs(n);
	//print_subspace(attrs);
	explore_subspace(interest,cont,cattrs,attrs,pairs,k);
	delete_list(attrs);
	delete_list_attr(cattrs);
	return pairs;
}

void print_attribute(attribute* attr){
	if(attr->direction == 1)
		printf("(%s,->)\n",attr->name);
	else
		printf("(%s,<-)\n",attr->name);
}

void print_subspace(list* subspace){
	int i;
	member* m = subspace->head;

	for(i=0;i<subspace->num;i++){
		print_attribute(m->node);
		m = m->next;
	}
}

void print_pair(pair* pa){
	printf("context is\n");
	print_context(pa->cont);
	printf("subspace is\n");
	print_subspace(pa->subspace);
	printf("score is %f\n",pa->score);
	
}


void print_pairs(list* pairs){
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num;i++){
		print_pair(m->node);
		printf("\n");
		m = m->next;
	}
}

void print_pair_n(pair* pa,char* interest,list* ene){
	printf("context is\n");
	print_context(pa->cont);
	printf("subspace is\n");
	print_subspace(pa->subspace);
	printf("score is %f\n",pa->score);
	member* m = ene->head;
	//member* ms = pa->subspace->head;
	int i,j;
	for(i=0;i<ene->num;i++){
		edge* e = m->node;
		member* ms = pa->subspace->head;
		for(j=0;j<pa->subspace->num;j++){
			attribute* attr = ms->node;
			if(strcmp(e->name,attr->name) == 0){
				if(attr->direction == 0)
					printf("%s - %s -> %s\n",((node*)e->from)->name,e->name,interest);
				else
					printf("%s - %s -> %s\n",interest,e->name,((node*)e->to)->name);
				
			}
			ms = ms->next;
		}
		m = m->next;
	}
}
	
void print_pairs_n(list* pairs,char* interest, list* ene){
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num;i++){
		print_pair_n(m->node,interest,ene);
		printf("\n");
		m = m->next;
	}
}

/*void delete_pair(pair* pa){
	delete_list(pa->context);
	delete_subspace(pa->subspace);
	free(pa);
}*/

void delete_pairs(list* pairs){
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num;i++){
		delete_pair(m->node);
		member* t = m;
		m = m->next;
		free(t);
	}
	free(pairs);
}


void delete_context(context* cont){
	delete_list(cont->nodes);
	free(cont);
}

void delete_contexts(list* lis){
	int i;
	member* m = lis->head;
	for(i=0;i<lis->num;i++){
		context* cont = m->node;
		delete_context(cont);
		m = m->next;
	}
	delete_list(lis);
}