#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "pattern.h"



list* discard_irrelevant_matches(list* matches,char* interest){
	int i;
	member* m = matches->head;
	int num = matches->num;
	for(i=0;i<num;i++){
		graph* match = m->node;
		if(find_n_byname(match->nl,interest) == NULL){
			member* t = m;
			m = m->next;
			delete_member_g(matches,t);
		}
		else
			m = m->next;
	}
}

list* find_good_edges(graph* match,graph* g){
	elist* elt = match->el;
	nlist* nlt = match->nl;
	edge* t = g->el->head;
	int i;
	list* edgs = create_list();
	for(i=0;i<g->el->num;i++){
		if(exists_e(match->el,t) == 0){
			if( (find_n_byname(match->nl,((node*)t->from)->name) != NULL) || (find_n_byname(match->nl,((node*)t->to)->name) != NULL) ){
				add_member(edgs,t);
			}
		}
		t = t->next;
	}
	return edgs;
}

list* find_children(char* interest,graph* p,graph* g,list* matches){
	list* cp = create_list();
	discard_irrelevant_matches(matches,interest);
	int i;
	member* m = matches->head;
	//printf("the number of relevant matches are %d\n",matches->num);
	for(i=0;i<matches->num;i++){
		graph* match = m->node;
		list* em = find_good_edges(match,g);
		member* me = em->head;
		int j;
		//printf("the number of good edges is %d\n",em->num);
		for(j=0;j<em->num;j++){
			edge* edg = me->node;
			node* n1 = edg->from;
			node* n2 = edg->to;
			int np1 = find_pn_byname(match->nl,n1->name);
			int np2 = find_pn_byname(match->nl,n2->name);
			if(np1 != -1 && np2 != -1){
				graph* np = copy_graph(p);
				n1 = find_n_byplace(p->nl,np1);
				n2 = find_n_byplace(p->nl,np2);
				add_edge_g(np,n1->name,n2->name,edg->name);
				add_member(cp,np);
			}
			else if(np1 != -1){
				graph* np = copy_graph(p);
				n1 = find_n_byplace(p->nl,np1);
				variable* v = n2->variable;
				add_node_vg(np,n2->name,v->name);
				add_edge_g(np,n1->name,n2->name,edg->name);
				add_member(cp,np);
				np = copy_graph(p);
				char nn[100];
				sprintf(nn,"?%d",find_next_qm(p));
				add_node_vg(np,nn,v->name);
				add_edge_g(np,n1->name,nn,edg->name);
				add_member(cp,np);
			}
			else{
				graph* np = copy_graph(p);
				n2 = find_n_byplace(p->nl,np2);
				variable* v = n1->variable;
				add_node_vg(np,n1->name,v->name);
				add_edge_g(np,n1->name,n2->name,edg->name);
				add_member(cp,np);
				np = copy_graph(p);
				char nn[100];
				sprintf(nn,"?%d",find_next_qm(p));
				add_node_vg(np,nn,v->name);
				add_edge_g(np,nn,n2->name,edg->name);
				add_member(cp,np);
			}
			me = me->next;
		}
		m = m->next;
		delete_list(em);
	}
	return cp;
}


float score_pattern(char* interest,graph* p,graph* g){//CHANGE
	//printf("in score pattern\n");
	list* matches = match(g,p);
	//printf("found number of matches they are %d\n",matches->num);
	list* contexts = find_contexts(g,p,matches,interest);
	//printf("found number of contexts they are %d\n",contexts->num);
	int i;
	member* m = contexts->head;
	float max=0;
	for(i=0;i<contexts->num;i++){
		context* cont = m->node;
		list* conts = cont->nodes;
		float t = ((float) 1)  -  (((float) 1)/conts->num);
		if(t > max)
			max = t;
		m = m->next;
	}
	delete_contexts(contexts);
	//delete_list_list(contexts);//CHANGE
	delete_list_g(matches);
	return max;
}

void add_member_sort_ps(list* pairs,pattern_score* pa){
	member* m = malloc(sizeof(member));
	m->node = pa;
	if(pairs->num == 0){
		pairs->head = m;
		pairs->tail = m;
		m->next = m;
	}
	else if(pairs->num == 1){
		pattern_score* p = pairs->head->node;
		if(p->score > pa->score){
			pairs->head->next = m;
			pairs->tail = m;
			m->next = pairs->head;
		}
		else{
			pairs->head->next = m;
			m->next = pairs->head;
			pairs->head = m;
			pairs->tail = m->next;
		}
	}
	else{
		pattern_score* p = pairs->head->node;
		pattern_score* lp = pairs->tail->node;
		if(p->score < pa->score){
			m->next = pairs->head;
			pairs->tail->next = m;
			pairs->head = m;
		}
		else if(lp->score > pa->score){
			m->next = pairs->head;
			pairs->tail->next = m;
			pairs->tail = m;
		}
		else{
			member* m1 = pairs->head;
			member* m2 = m1->next;
			int i;
			for(i=0;i<pairs->num-1;i++){
				pattern_score* p1 = m1->node;
				pattern_score* p2 = m2->node;
				if(p1->score >= pa->score && p2->score <= pa->score){
					m1->next = m;
					m->next = m2;
					break;
				}
				m1 = m1->next;
				m2 = m2->next;
			}
		}
	}	
	pairs->num++;
}

void delete_pattern_score(pattern_score* ps){
	delete_graph(ps->pattern);
	free(ps);
}

void delete_last_pattern_score(list* pairs){
	if(pairs->num == 1){
		delete_pattern_score(pairs->head->node);
		free(pairs->head);
		pairs->num--;
		pairs->head = NULL;
		pairs->tail = NULL;
		return;
	}
	int i;
	member* m = pairs->head;
	for(i=0;i<pairs->num-2;i++)
		m = m->next;
	delete_pattern_score(m->next->node);
	free(m->next);
	m->next = pairs->head;
	pairs->tail = m;
	pairs->num--;
}


list* pattern_generator(char* interest,graph* p,list* matches,int w, graph* g){
	list* children = find_children(interest,p,g,matches);
	//printf("children found,number of children %d \n",children->num);
	list* p_s = create_list();
	member* m = children->head;
	int i,num;
	num = children->num;
	for(i=0;i<num;i++){
		graph* t = m->node;
		pattern_score* ps = malloc(sizeof(pattern_score));
		ps->pattern = t;
		clock_t tim = clock();
		ps->score = score_pattern(interest,t,g);
		tim = clock() - tim;
		double ttk = ((double)tim)/CLOCKS_PER_SEC;
		//printf("scoring child %d took %f seconds\n",i+1,ttk);
		add_member_sort_ps(p_s,ps);
		m = m->next;
		delete_member_without(children,find_member(children,t));
	}
	delete_list(children);
	while(p_s->num > w)
		delete_last_pattern_score(p_s);
	return p_s;
}
