#include "list.h"
#include "stdlib.h"

list* create_list(){
	list* t = malloc(sizeof(list));
	t->head = NULL;
	t->tail = NULL;
	t->num = 0;
	return t;
}

member* add_member(list* list,void* n){
	member* t = malloc(sizeof(member));
	t->node = n;
	if(list->num == 0){
		list->head = t;
		list->tail = t;
		t->next = t;
	}
	else{
		t->next = list->head;
		list->tail->next = t;
		list->tail = t;
	}
	list->num++;
	return t;
}

void delete_list(list* list){
	int i;
	member* t;
	for(i=0;i<list->num;i++){
		t = list->head;
		list->head = list->head->next;
		free(t);
	}
	free(list);
}

void* find_m_bynum(list* list,int num){
	member* t = list->head;
	int i;
	for(i=0;i<num;i++){
		t = t->next;
	}
	return t->node;
}

void print_list_edge(list* list){
	int i;
	for(i=0;i<list->num;i++){
		print_edge(find_m_bynum(list,i));
	}
}


void delete_member(list* list,member* mem){
	int i;
	member *t= list->head;
	member *tn= list->tail;
	for(i=0;i<list->num;i++){
		if(t == mem){
			if(i==0){
				list->head = t->next;
				list->tail->next = t->next;
				delete_list(mem->node);
				free(mem);
			}
			else{
				tn->next = t->next;
				delete_list(mem->node);
				free(mem);
			}
			break;
		}
		t = t->next;
		tn = tn->next;
	}
	list->num--;
}


void delete_member_without(list* list,member* mem){
	if(list->num == 0)
		return;
	if(list->num == 1){
		free(list->head);
		list->head = NULL;
		list->tail = NULL;
		list->num--;
		return;
	}
	int i;
	member *t= list->head;
	member *tn= list->tail;
	for(i=0;i<list->num;i++){
		if(t == mem){
			if(i==0){
				list->head = t->next;
				list->tail->next = t->next;
				//delete_list(mem->node);
				free(mem);
			}
			else{
				tn->next = t->next;
				if(i== list->num-1)
					list->tail = tn;
				//delete_list(mem->node);
				free(mem);
			}
			break;
		}
		t = t->next;
		tn = tn->next;
	}
	list->num--;
}

member* find_member(list* list,void* p){
	int i;
	member* m = list->head;
	for(i=0;i<list->num;i++){
		if(p == m->node)
			return m;
		m = m->next;
	}
	return NULL;
}
