#include "node.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

nlist* create_nlist(){
	nlist* t = malloc(sizeof(nlist));
	t->head = NULL;
	t->tail = NULL;
	t->num = 0;
	return t;
}

node* create_node(char* name){
	node* t = malloc(sizeof(node));
	strcpy(t->name,name);
	t->next = NULL;
	t->list = create_list();
	return t;
}

node* create_node_v(char* name,char* varname,void* list){
	node* t = malloc(sizeof(node));
	strcpy(t->name,name);
	t->next = NULL;
	variable* v = find_v_byname(list,varname);
	t->variable = v;
	add_member(v->list,t);
	t->list = create_list();
	return t;
}

node* add_node(nlist* list,node* t){
	if(list->num == 0){
		list->head = t;
		list->tail = t;
		t->next = t;
	}
	else{
		t->next = list->head;
		list->tail->next = t;
		list->tail = t;
	}
	list->num++;
	return t;
}

node* add_node_m(nlist* list,char* name){
	return add_node(list,create_node(name));
}

node* add_node_vm(nlist* list,char* name,char* varname,void* lis){
	return add_node(list,create_node_v(name,varname,lis));
}

void delete_nlist(nlist* list){
	int i;
	node* t;
	for(i=0;i<list->num;i++){
		t = list->head;
		list->head = list->head->next;
		//free(t->list);
		delete_list(t->list);
		free(t);
	}
	free(list);
}


node* find_n_byname(nlist* list,char* name){
	node* t = list->head;
	int i;
	for(i=0;i<list->num;i++){
		if(strcmp(t->name,name) == 0)
			return t;
		t = t->next;
	}
	return NULL;
}

int find_pn_byname(nlist* list,char* name){
	node* t = list->head;
	int i;
	for(i=0;i<list->num;i++){
		if(strcmp(t->name,name) == 0)
			return i;
		t = t->next;
	}
	return -1;
}

node* find_n_byplace(nlist* list,int num){
	node* t = list->head;
	int i;
	for(i=0;i<num;i++){
		t = t->next;
	}
	return t;
}

void print_node(node* n){
	printf("%s\n",n->name);
}

void print_nlist(nlist* list){
	int i;
	node* t = list->head;
	for(i=0;i<list->num;i++){
		print_node(t);
		t = t->next;
	}
}

void print_node_edges(node* n){
	print_list_edge(n->list);
}

void print_node_ledges(nlist* list){
	int i;
	node* t = list->head;
	for(i=0;i<list->num;i++){
		print_node_edges(t);
		t = t->next;
	}
}

int has_dup_n(nlist* list){
	int i,j;
	node* t1 = list->head;
	for(i=0;i<list->num;i++){
		node* t2 = t1->next;
		for(j=i+1;j<list->num;j++){
			if(strcmp(t1->name,t2->name) ==0)
				return 1;
		}
	}
	return 0;
}

/*void print_list_n(list* list){
	int i;
	member* t = list->head;
	for(i=0;i<list->num;i++){
		printf("%s\n",member->node->name);
		t = t->next;
	}
}*/
