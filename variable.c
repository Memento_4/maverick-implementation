#include "variable.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


vlist* create_vlist(){
	vlist* t = malloc(sizeof(vlist));
	t->head = NULL;
	t->tail = NULL;
	t->num = 0;
	return t;
}

variable* create_variable(char* name,int flag){
	variable* t = malloc(sizeof(variable));
	strcpy(t->name,name);
	t->flag = flag;
	t->next = NULL;
	t->list = create_list();
	return t;
}

variable* add_variable(vlist* list,variable* t){
	if(list->num == 0){
		list->head = t;
		list->tail = t;
		t->next = t;
	}
	else{
		t->next = list->head;
		list->tail->next = t;
		list->tail = t;
	}
	list->num++;
	return t;
}

variable* add_variable_m(vlist* list,char* name,int flag){
	return add_variable(list,create_variable(name,flag));
}

void delete_vlist(vlist* list){
	int i;
	variable* t;
	for(i=0;i<list->num;i++){
		t = list->head;
		list->head = list->head->next;
		//free(t->list);
		delete_list(t->list);
		free(t);
	}
	free(list);
}


variable* find_v_byname(vlist* list,char* name){
	variable* t = list->head;
	int i;
	for(i=0;i<list->num;i++){
		if(strcmp(t->name,name) == 0)
			return t;
		t = t->next;
	}
	return NULL;
}

variable* find_v_byplace(vlist* list,int num){
	variable* t = list->head;
	int i;
	for(i=0;i<num;i++){
		t = t->next;
	}
	return t;
}

void print_variable(variable* v){
	printf("%s\n",v->name);
}

void print_vlist(vlist* list){
	int i;
	variable* t = list->head;
	for(i=0;i<list->num;i++){
		print_variable(t);
		t = t->next;
	}
}
