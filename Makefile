ALL 		= mainf epictest maint mainf.o maint.o epictest.o pattern.o node.o edge.o list.o context.o grapht.o variable.o
OBJS 		= epictest.o node.o edge.o list.o context.o grapht.o variable.o
TESTOBJS 	= maint.o pattern.o node.o edge.o list.o context.o grapht.o variable.o
OBJSF		= mainf.o pattern.o node.o edge.o list.o context.o grapht.o variable.o
OUT  		= epictest
OUTT		= maint
OUTF		= mainf


$(OUTF): $(OBJSF)
	gcc -o $(OUTF) $(OBJSF) -lm -lpthread -g

$(OUTT): $(TESTOBJS)
	gcc -o $(OUTT) $(TESTOBJS) -lm -lpthread -g

$(OUT): $(OBJS)
	gcc -o $(OUT) $(OBJS) -lm -lpthread -g



epictest.o: epictest.c
	gcc -g -c epictest.c

maint.o: maint.c
	gcc -g -c maint.c

mainf.o: mainf.c
	gcc -g -c mainf.c

node.o: node.c
	gcc -g -c node.c

edge.o: edge.c
	gcc -g -c edge.c

list.o: list.c
	gcc -g -c list.c -lm

grapht.o: grapht.c
	gcc -g -c grapht.c

context.o: context.c
	gcc -g -c context.c

variable.o: variable.c
	gcc -g -c variable.c

pattern.o: pattern.c
	gcc -g -c pattern.c

clean:
	rm -f $(ALL)
