#ifndef CONTEXT_H
#define CONTEXT_H

#include <stdbool.h>
#include "grapht.h"

typedef struct attribute{
	char name[100];
	int direction;
}attribute;


typedef struct projection{
	list* nodes;
	list* pjs;
	float score;
}projection;

typedef struct context{
	char pat[1000];
	list* nodes;
	int tvar;
}context;

typedef struct pair{
	context* cont;
	list* subspace;
	float score;
}pair;

void delete_last_pair(list* pairs);
void add_member_sort_pair(list* pairs,pair* pa);
list* find_contexts(graph* g,graph* p,list* matches,char* interest);
int exists(char* name,list* context);
void delete_list_list(list* lis);
void print_context(context* cont);
void print_contexts(list* lis);
bool isa_duplicate(list* cont1,list* cont2);
void delete_member_c(list* list,member* mem);
void cull_duplicate_contexts(list* contexts);
float scf(list* context,list* subspace,char* interest);
list* find_attrs(node* n);
void delete_list_attr(list* attrs);
void print_pairs(list* pairs);
void delete_pairs(list* pairs);
void print_pairs_n(list* pairs,char* interest, list* ene);
list* exceptionality_evaluator(char* interest,context* cont,int k,nlist* ns);
void print_subspace(list* subspace);
void delete_context(context* cont);
void delete_contexts(list* lis);

#endif

