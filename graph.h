#include <stdbool.h>

typedef struct node{
	char name[100];
	char varname[100];
	int flag;
}node;

typedef struct edge{
	node node1;
	char attribute[100];
	node node2;
}edge;

typedef struct variable{
	char name[100];
	int num;
	node* nodes;
}variable;

typedef struct graph{
	node* nodes;
	int n_num;
	edge* edges;
	int e_num;
	variable* variables;
	int v_num;
}graph;

typedef struct context{
	node* nodes;
	int num;
}context;

typedef struct match{
	node* nodes;
	int n_num;
	edge* edges;
	int e_num;
}match;

typedef struct matchs{
	match* matches;
	int num; 
}matchs;

typedef struct attributes{
	char** attrs;
	int flag;
	int num;
}attributes;

void printvariables(graph* g);
void printnodes(graph* g);
void printedges(graph* g);
void printmatchs(matchs* ms);
void printmatch(match* m);


void m_rb(matchs* ms,int n);
matchs* find_all_possible_matches(graph* g,match* matches,int num);
//void readvars(graph* g,char* file);
void readgraph(graph* g, char* file);
void remgraph(graph* g);
