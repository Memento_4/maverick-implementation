#include "edge.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


elist* create_elist(){
	elist* t = malloc(sizeof(elist));
	t->head = NULL;
	t->tail = NULL;
	t->num = 0;
	return t;
}


char* turn_etos(edge* e){//turn edge to string
	char* st = malloc(1000);
	sprintf(st,"%s - %s -> %s,",((node*)e->from)->name,e->name,((node*)e->to)->name);
	//printf("printing edge %s\n",st);
	return st;
}

edge* create_edge(char* from,char* to,char* attrname , void* list){
	edge* t = malloc(sizeof(edge));
	node* n;
	n = find_n_byname(list,from);
	add_member(n->list,t);
	t->from = n;
	n = find_n_byname(list,to);
	add_member(n->list,t);
	t->to = n;
	strcpy(t->name,attrname);
	t->next = NULL;
	return t;
}

edge* add_edge(elist* list,edge* t){
	if(list->num == 0){
		list->head = t;
		list->tail = t;
		t->next = t;
	}
	else{
		t->next = list->head;
		list->tail->next = t;
		list->tail = t;
	}
	list->num++;
	return t;
}

edge* add_edge_m(elist* list,char* from,char* to,char* attrname,void* lis ){
	return add_edge(list,create_edge(from,to,attrname,lis));
}

void delete_elist(elist* list){
	int i;
	edge* t;
	for(i=0;i<list->num;i++){
		t = list->head;
		list->head = list->head->next;
		free(t);
	}
	free(list);
}

//edge* find_e_byname(nlist* list,char* name);

edge* find_e_byplace(elist* list,int num){
	edge* t = list->head;
	int i;
	for(i=0;i<num;i++){
		t = t->next;
	}
	return t;
}

void print_edge(edge* e){
	//node* t = e->from;
	//node* t2 = e->to;
	printf("%s %s %s\n",((node*) e->from)->name,e->name,((node*) e->to)->name);
}

void print_elist(elist* list){
	int i;
	edge* t = list->head;
	for(i=0;i<list->num;i++){
		print_edge(t);
		t = t->next;
	}
}

int edge_cmp(edge* one,edge* two){
	if(strcmp(((node*)one->from)->name,((node*)two->from)->name)!=0)
		return 1;
	if(strcmp(((node*)one->to)->name,((node*)two->to)->name)!=0)
		return 1;
	if(strcmp(one->name,two->name)!=0)
		return 1;
	return 0;
}

int exists_e(elist* list, edge* e){
	int i;
	edge* t = list->head;
	for(i=0;i<list->num;i++){
		if(edge_cmp(t,e) == 0)
			return 1;
		t = t->next;
	}
	return 0;
}

int proper_es(elist* es,elist* judge){
	int i,j;
	edge* t = es->head;
	for(i=0;i<es->num;i++){
		if((((node*)t->from)->name[0] == '?') || ((node*)t->to)->name[0] == '?'){
			t = t->next;
			continue;
		}
		if(exists_e(judge,t) == 0)
			return 0;
		t = t->next;
	}
	return 1;
}

/*int proper_es(elist* es,elist* judge){
	int i,j;
	edge* t = es->head;
	for(i=0;i<es->num;i++){
		if(exists_e(judge,t) == 0)
			return 0;
		t = t->next;
	}
	return 1;
}*/

