#ifndef EDGE_H
#define EDGE_H
#include "node.h"
#include "list.h"

typedef struct edge{
	void* from;
	void* to;
	char name[100];
	struct edge* next;
}edge;

typedef struct elist{
	edge* head;
	edge* tail;
	int num;
}elist;


elist* create_elist();

edge* create_edge(char* from,char* to,char* attrname,void* list );

edge* add_edge(elist* list,edge* t);

edge* add_edge_m(elist* list,char* from,char* to,char* attrname,void* lis);

void delete_elist(elist* list);

//edge* find_e_byname(nlist* list,char* name);

edge* find_e_byplace(elist* list,int num);

void print_edge(edge* e);

void print_elist(elist* list);

int edge_cmp(edge* one,edge* two);

int exists_e(elist* list, edge* e);

int proper_es(elist* es,elist* judge);

char* turn_etos(edge* e);
#endif
